FROM nvidia/cuda:10.1-runtime-ubuntu18.04

ARG ENV_NAME=default
ARG ENV_VERSION=vENV_VERSION=v52

RUN apt-get -qq update \
    && apt-get -y -qq install \
    curl \
    wget \
    ssh \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /opt/conda/envs/${ENV_NAME} \
    && curl -sS https://conda-envs.proteinsolver.org/default/${ENV_NAME}-${ENV_VERSION}.tar.gz | tar -xz -C /opt/conda/envs/${ENV_NAME}
