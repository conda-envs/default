# default-vv52

Use [`conda-pack`](https://conda.github.io/conda-pack/) to create a tarball with a set of conda packages that are needed for most tasks.

## Notes

The master environment used for development work should be created with:

```bash
conda install -y -q nodejs jupyterlab jupyterlab_code_formatter isort black
```

Each of the sub-environments can be created using:

```bash
tar -xzf {environment_name}.tar.gz -C /path/to/{environment_name}
source /path/to/{environment_name}/bin/activate
conda-unpack
```

Additional environments can be registered with this environment using:

```bash
conda activate {environment_name}
ipython kernel install --user --name {environment_name}
jupyter kernelspec list
```

They can be unregistered with this environment using:

```bash
conda deactivate
jupyter kernelspec uninstall {environment_name}
```

If we wish for the kernel startup script to activate the conda environment before starting the kernel, we should edit `~/.jupyter/data/kernels/python3/kernel.json` to use [`conda_ipykernel_launcher.sh`](https://gitlab.com/ostrokach-forge/conda_ipykernel_launcher) instead of `ipykernel`.

For example:

```json
{
 "argv": [
  "/home/username/env/bin/conda_ipykernel_launcher.sh",
  "{connection_file}"
 ],
 "display_name": "Python 3",
 "language": "python"
}
```
