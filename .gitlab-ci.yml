default:
  image: ubuntu:20.04

stages:
  - build
  - deploy

variables:
  ENVIRONMENT_NAME: "default"

build:
  stage: build
  tags:
    - arbutus
  before_script:
    # Install global dependencies
    - apt-get update -y -qq -o=Dpkg::Use-Pty=0
    - apt-get install -y -qq -o=Dpkg::Use-Pty=0 curl rsync gettext-base
    # Install ssh client
    - "which ssh-agent || ( apt-get install -y -qq -o=Dpkg::Use-Pty=0 openssh-client -y )"
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$KNOWN_HOSTS" >> ~/.ssh/known_hosts
    # Test that ssh client works
    - ssh gitlab@arbutus.proteinsolver.org "echo hello"
    # Download and install miniconda
    - curl -o miniconda.sh -L -C - --retry 999 --retry-max-time 0 --no-progress-meter
      https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-x86_64.sh
    - sh miniconda.sh -b -p /opt/conda
    - source /opt/conda/etc/profile.d/conda.sh
    - conda activate root
    - conda config --set channel_priority flexible # TODO: strict
    - conda config --set pip_interop_enabled false
    - conda install -n root -c conda-forge -y -q mamba conda-pack  # constructor
  script:
    # Install GPU-enabled packages by default
    - export CONDA_OVERRIDE_CUDA="11.8"
    
    # Create conda environment
    # NB: mamba does not seem to work with the custom kimlab channels
    - mamba env create -q -n ${ENVIRONMENT_NAME} -f environment.yml
    - conda activate ${ENVIRONMENT_NAME}

    # For Julia
    - pushd /opt/conda/lib
    - ln -s libmbedcrypto.so libmbedcrypto.so.5
    - ln -s libmbedtls.so libmbedtls.so.13
    - ln -s libmbedx509.so libmbedx509.so.1
    - popd

    # Install extras
    # - envsubst < environment-extras.yml > environment-extras-subst.yml
    # - conda env update -q -n ${ENVIRONMENT_NAME} -f environment-extras-subst.yml

    # Install pypi packages
    # - pip install -r requirements.txt --no-use-pep517 -U
    # - pip install -r requirements.txt -U
    # - curl -sSL http://github.com/NVIDIA/DeepLearningExamples/archive/88e5298421453d69e9683e970bc7b5e97fa48c3b.tar.gz | tar xz
    # - pip install DeepLearningExamples-*/PyTorch/DrugDiscovery/SE3Transformer/
    # - conda list -n ${ENVIRONMENT_NAME}

    # Tests
    - python -c "import torch"

    # Make everything writable
    - chmod ug+rwX -R /opt/conda/envs/${ENVIRONMENT_NAME}

    # Show the created environment
    - conda deactivate
    - conda list -n ${ENVIRONMENT_NAME}

    # Pack the environment
    - if [[ -d /share/conda-envs/default ]] ;
      then 
      OUTPUT_DIR=/share/conda-envs/default ;
      else
      OUTPUT_DIR=$(pwd) ;
      fi
    - export ENVIRONMENT_FILE=${OUTPUT_DIR}/${ENVIRONMENT_NAME}-${CI_COMMIT_REF_NAME}.tar.gz
    - conda pack -q -n ${ENVIRONMENT_NAME} -o ${ENVIRONMENT_FILE}
    - ls -lSh ${ENVIRONMENT_FILE}

    # Upload the environment to shared folder
    - if [[ ! -d /share/conda-envs/default ]] ;
      then
      rsync -rv --info=progress2 -p --chmod=ug=rwX,o=rX "${ENVIRONMENT_FILE}" gitlab@arbutus.proteinsolver.org:/share/conda-envs/${ENVIRONMENT_NAME}/ ;
      fi
  except:
    variables:
      - $UPDATE_IMAGES

deploy:
  stage: deploy
  image:
    name: docker:latest
  services:
    - docker:dind
  tags:
    - east-cloud
  script:
    - if [[ ! -z ${CI_COMMIT_TAG} ]] ; then
      IMAGE_TAG=${CI_COMMIT_TAG} ;
      else
      IMAGE_TAG="latest" ;
      fi
    - echo "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
    - docker login registry.gitlab.com -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
    - docker build -t "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}" .
    - docker push "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
  # only:
  #   - tags
  only:
    variables:
      - $UPDATE_IMAGES
